const servicePrefix = require("../constants").servicePrefix;
const predictionDataPrefix = require("../constants").predictionDataPrefix;
const predictionDataRecordsPrefix = require("../constants").predictionDataRecordsPrefix;
const storageErrorResponseBody = require("../../utilities").storageErrorResponseBody;

handler = async (request, h) => {
  const storage = request.server.app.aiLibrary.storage;
  const id = request.params.id;
  const fileKey = `${servicePrefix}/${predictionDataPrefix}/${id}/duplicate-prediction-data.json`;
  const fileDir = `${servicePrefix}/${predictionDataPrefix}/${id}/${predictionDataRecordsPrefix}`;
  const includeRecords = request.query.include === "records";

  try {
    let data = await storage.readJson(fileKey);
    if (includeRecords) {
      data.records = await storage.readJsonDir(fileDir);
    }

    return h.response({
      metadata: {
        type: "DuplicatePredictionData",
        async: {
          status: "success"
        }
      },
      data
    }).code(200);
  }
  catch (err) {
    console.error(err);
    let responseBody = storageErrorResponseBody(err);
    return h.response(responseBody).code(responseBody.statusCode);
  }
};

module.exports.handler = handler;