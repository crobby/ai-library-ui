addPredictionDataRoutes = (server) => {
require("./records")(server);

  server.route({
    method: ["GET"],
    path: "/api/duplicate/prediction-data/{id}",
    handler: require("./get").handler
  });


  server.route({
    method: ["POST"],
    path: "/api/duplicate/prediction-data",
    handler: require("./post").handler,
  });
};

module.exports = addPredictionDataRoutes;