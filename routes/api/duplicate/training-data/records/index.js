addDataRecordRoutes = (server) => {
  server.route({
    method: ["GET"],
    path: "/api/duplicate/training-data/{setId}/records/{id}",
    handler: require("./get").handler
  });

  server.route({
    method: ["POST"],
    path: "/api/duplicate/training-data/{setId}/records",
    handler: require("./post").handler,
    options: {
      validate: require("./post").validate
    }
  });
};

module.exports = addDataRecordRoutes;