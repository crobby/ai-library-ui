addTrainingDataRoutes = (server) => {
  require("./records")(server);

  server.route({
    method: ["GET"],
    path: "/api/flake/training-data/{id}",
    handler: require("./get").handler
  });


  server.route({
    method: ["POST"],
    path: "/api/flake/training-data",
    handler: require("./post").handler,
  });
};

module.exports = addTrainingDataRoutes;