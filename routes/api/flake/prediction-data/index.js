addPredictionDataRoutes = (server) => {
require("./records")(server);

  server.route({
    method: ["GET"],
    path: "/api/flake/prediction-data/{id}",
    handler: require("./get").handler
  });


  server.route({
    method: ["POST"],
    path: "/api/flake/prediction-data",
    handler: require("./post").handler,
  });
};

module.exports = addPredictionDataRoutes;