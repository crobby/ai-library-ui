const urlJoin = require("url-join");
const Wreck = require("wreck");
const asyncStatusFromAiLibrary = require("../../utilities").asyncStatusFromAiLibrary;

const servicePrefix = require("../constants").servicePrefix;
const predictionsPrefix = require("../constants").predictionsPrefix;
const predictionRecordsPrefix = require("../constants").predictionRecordsPrefix;
const storageErrorResponseBody = require("../../utilities").storageErrorResponseBody;

handler = async (request, h) => {
  const storage = request.server.app.aiLibrary.storage;
  const id = request.params.id;
  const includeRecords = request.query && request.query.include === "records";

  const predictionDir = `${servicePrefix}/${predictionsPrefix}/${id}`;
  const predictionJsonLoc = `${predictionDir}/prediction.json`;
  const predictionOutputLoc = `${predictionDir}/${predictionRecordsPrefix}`;

  let httpStatusCode = 200;
  let predictionsExist = false;
  let data = null;

  try {
    data = await storage.readJson(predictionJsonLoc);
    if (includeRecords) {
      data.records = await storage.readJsonDir(predictionOutputLoc);
      predictionsExist = data.records && data.records.length > 0;
    } else {
      let fileList = await storage.ls(predictionOutputLoc, 1); //check for 1 record to exist
      predictionsExist = fileList.Contents && fileList.Contents.length > 0;
    }
  }
  catch (err) {
    console.error(err);
    let responseBody = storageErrorResponseBody(err);
    return h.response(responseBody).code(responseBody.statusCode);
  }

  if (predictionsExist) {
    return h.response({
      metadata: {
        type: "FlakePrediction",
        async: {
          status: "success"
        }
      },
      data
    }).code(httpStatusCode);
  }

  const pollRequestUrl = urlJoin(request.server.app.aiLibrary.url, "poll-status?blocking=true&result=true");
  const headers = {Authorization: request.server.app.aiLibrary.basicAuthHeader, ...request.headers};
  const name = request.params.id;

  let jobStatus = null;

  delete headers.host;
  delete headers["content-length"];

  try {
    const pollingResponse = await Wreck.request(
      "POST",
      pollRequestUrl,
      {
        payload: { name },
        headers: headers,
        rejectUnauthorized: false
      });

    const r = await Wreck.read(pollingResponse, {json: true});
    if (pollingResponse.statusCode > 299) {
      console.error(request);
      return h.response({r}).code(pollingResponse.statusCode);
    }

    jobStatus = asyncStatusFromAiLibrary(r);

    //if the job is successful but the file is missing, we return 404;
    if (!predictionsExist && jobStatus === "success") {
       return h.response().code(404);
    }


    return h.response({
      metadata: {
        type: "FlakePrediction",
        async: {
          status: jobStatus
        }
      },
      data
    }).code(httpStatusCode);
  }
  catch (err) {
    console.error(err);
    return h.response({err}).code(err.statusCode);
  }
};

module.exports.handler = handler;
