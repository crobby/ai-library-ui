module.exports.servicePrefix = "flake";
module.exports.trainingDataPrefix = "training-data";
module.exports.trainingDataRecordsPrefix = "records";
module.exports.modelsPrefix = "models";
module.exports.predictionDataPrefix = "prediction-data";
module.exports.predictionDataRecordsPrefix = "records";
module.exports.predictionsPrefix = "predictions";
module.exports.predictionRecordsPrefix = "records";
