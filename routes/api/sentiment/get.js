const urlJoin = require("url-join");
const Wreck = require("wreck");

handler = async (request, h) => {
  const apiRequestUrl = urlJoin(request.server.app.aiLibrary.activationsUrl, request.params.id);
  const headers = {Authorization: request.server.app.aiLibrary.basicAuthHeader, ...request.headers};

  delete headers.host;
  delete headers["content-length"];

  try {
    const res = await Wreck.request(
      "GET",
      apiRequestUrl,
      {
        headers: headers,
        rejectUnauthorized: false
      });

    const r = await Wreck.read(res, {json: true});
    if (res.statusCode > 299) {
      console.error(request);
      return h.response(r).code(res.statusCode);
    }

    return {
      metadata: {
        type: "Sentiment",
        async: {
          status: r.response.status
        }
      },
      data: {
        id: r.activationId,
        entities: r.response.result.sentiment
      }
    };
  }
  catch (err) {
    console.error(err);
    return h.response(err).code(err.statusCode);
  }
};

module.exports.handler = handler;
