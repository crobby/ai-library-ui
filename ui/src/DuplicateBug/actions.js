import axios from "axios"
import urljoin from "url-join";

import asyncSleep from "../Utilities/asyncSleep";
import { createAxiosErrorNotification } from "../Notifications/actions";



export const UPDATE_PREDICTION_UI = "UPDATE_PREDICTION_UI";
export const updatePredictionUI = (ui) => ({
  type: UPDATE_PREDICTION_UI,
  payload: {ui}
});


export const RESET_DUPLICATE_PREDICTION = "RESET_DUPLICATE_PREDICTION";
export const resetDuplicatePrediction = () => ({
  type: RESET_DUPLICATE_PREDICTION,
  payload: {}
});

export const CREATE_DUPLICATE_PREDICTION_DATA_PENDING = "CREATE_DUPLICATE_PREDICTION_DATA_PENDING";
export const createDuplicatePredictionDataPending = (body) => ({
  type: CREATE_DUPLICATE_PREDICTION_DATA_PENDING,
  payload: {
    body
  }
});

export const CREATE_DUPLICATE_PREDICTION_DATA_FULFILLED = "CREATE_DUPLICATE_PREDICTION_DATA_FULFILLED";
export const createDuplicatePredictionDataFulfilled = (response) => ({
  type: CREATE_DUPLICATE_PREDICTION_DATA_FULFILLED,
  payload: {response}
});

export const CREATE_DUPLICATE_PREDICTION_DATA_RECORDS_PENDING = "CREATE_DUPLICATE_PREDICTION_DATA_RECORDS_PENDING";
export const createDuplicatePredictionDataRecordsPending = (body) => ({
  type: CREATE_DUPLICATE_PREDICTION_DATA_RECORDS_PENDING,
  payload: {body}
});

export const CREATE_DUPLICATE_PREDICTION_DATA_RECORDS_FULFILLED = "CREATE_DUPLICATE_PREDICTION_DATA_RECORDS_FULFILLED";
export const createDuplicatePredictionDataRecordsFulfilled = (response) => ({
  type: CREATE_DUPLICATE_PREDICTION_DATA_RECORDS_FULFILLED,
  payload: {response}
});

export const GET_DUPLICATE_PREDICTION_PENDING = "GET_DUPLICATE_PREDICTION_PENDING";
export const getDuplicatePredictionPending = (requestDate) => ({
  type: GET_DUPLICATE_PREDICTION_PENDING,
  payload: {requestDate}
});

export const GET_DUPLICATE_PREDICTION_FULFILLED = "GET_DUPLICATE_PREDICTION_FULFILLED";
export const getDuplicatePredictionFulfilled = (response) => ({
  type: GET_DUPLICATE_PREDICTION_FULFILLED,
  payload: {response}
});

export const GET_DUPLICATE_PREDICTION_REJECTED = "GET_DUPLICATE_PREDICTION_REJECTED";
export const getDuplicatePredictionRejected = (error) => ({
  type: GET_DUPLICATE_PREDICTION_REJECTED,
  payload: {error}
});

export const getDuplicatePrediction = (id) => {
  return async function (dispatch) {
    dispatch(getDuplicatePredictionPending(new Date()));
    try {
      let response = await axios.get(`/api/duplicate/predictions/${id}?include=records`);
      dispatch(getDuplicatePredictionFulfilled(response));
    }
    catch (error) {
      dispatch(createAxiosErrorNotification(error));
      dispatch(getDuplicatePredictionRejected(error));
    }
  }
};

export const CREATE_DUPLICATE_PREDICTION_PENDING = "CREATE_DUPLICATE_PREDICTION_PENDING";
export const createDuplicatePredictionPending = (body) => ({
  type: CREATE_DUPLICATE_PREDICTION_PENDING,
  payload: {body}
});

export const CREATE_DUPLICATE_PREDICTION_FULFILLED = "CREATE_DUPLICATE_PREDICTION_FULFILLED";
export const createDuplicatePredictionFulfilled = (response) => ({
  type: CREATE_DUPLICATE_PREDICTION_FULFILLED,
  payload: {response}
});

export const CREATE_DUPLICATE_PREDICTION_REJECTED = "CREATE_DUPLICATE_PREDICTION_REJECTED";
export const createDuplicatePredictionRejected = (error) => ({
  type: CREATE_DUPLICATE_PREDICTION_REJECTED,
  payload: {error}
});

export const createDuplicatePrediction = (bugs, modelId) => {
  return async function (dispatch) {
    try {
      let pd = await _createDuplicatePredictionData(dispatch, bugs);
      await asyncSleep(1000);
      dispatch(createDuplicatePredictionPending({modelId, predictionDataId: pd.id}));
      let response = await axios.post("/api/duplicate/predictions", {modelId, predictionDataId: pd.id});
      dispatch(createDuplicatePredictionFulfilled(response));
    }
    catch (error) {
      dispatch(createAxiosErrorNotification(error));
      dispatch(createDuplicatePredictionRejected(error));
    }
  }
};

async function _createDuplicatePredictionData(dispatch, bugs) {
  const predictionDataUrl = "/api/duplicate/prediction-data";
  const body = {name: "AI Library Demo"};
  dispatch(createDuplicatePredictionDataPending(body));
  let response = await axios.post(predictionDataUrl, body);
  dispatch(createDuplicatePredictionDataFulfilled(response));
  await asyncSleep(1000);

  let predictionData = response.data.data;
  let recordPromises = [];
  let requestBodies = [];

  bugs.forEach(bug => {
    recordPromises.push(
      axios.post(urljoin(predictionDataUrl, predictionData.id, "records"), bug)
    );
    requestBodies.push(bug);
  });

  dispatch(createDuplicatePredictionDataRecordsPending(requestBodies));
  let records = await Promise.all(recordPromises);
  dispatch(createDuplicatePredictionDataRecordsFulfilled(records));

  return predictionData;
}
