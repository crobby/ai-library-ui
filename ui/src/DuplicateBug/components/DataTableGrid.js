
import React from "react";
import { TableGrid } from "patternfly-react-extensions";

const idColSizes = {
  sm: 3
};
const titleColSizes = {
  sm: 4
};
const contentColSizes = {
  sm: 5
};

class DataTableGrid extends React.Component {


  constructor(props) {
    super(props);

    this.state = {
      sortField: "title",
      isAscending: true,
      items: props.data,
      selectedItem: null,
      selectedField: null,
      selectedItems: []
    }
  }

  onSelect = (item, field) => {
    this.setState({ selectedItem: item, selectedField: field });
  };

  renderItemRow = (item, index) => {
    const {selectType} = this.props;
    const {selectedItem, selectedField, selectedItems} = this.state;
    const selected = selectType === "checkbox" ? selectedItems.indexOf(item) >= 0 : selectedItem === item;
    return (
      <TableGrid.Row
        key={index}
        onClick={() => selectType === "row" && this.onSelect(item)}
        selected={(selectType === "row" || selectType === "checkbox") && selected}
        onToggleSelection={() => this.toggleSelection(item)}
      >
        <TableGrid.Col
          {...idColSizes}
          onClick={() => selectType === "cell" && this.onSelect(item, "id")}
          selected={selectType === "cell" && selected && selectedField === "id"}
        >
          {item.id || <span className="spinner spinner-xs spinner-inline"/>}
        </TableGrid.Col>
        <TableGrid.Col
          {...titleColSizes}
          onClick={() => selectType === "cell" && this.onSelect(item, "title")}
          selected={selectType === "cell" && selected && selectedField === "title"}
        >
          {item.title}
        </TableGrid.Col>
        <TableGrid.Col
          {...contentColSizes}
          onClick={() => selectType === "cell" && this.onSelect(item, "content")}
          selected={selectType === "cell" && selected && selectedField === "content"}
        >
          {item.content}
        </TableGrid.Col>
      </TableGrid.Row>
    );
  };

  render() {
    const { items, selectedItems, isAscending } = this.state;
    const { bordered, selectType } = this.props;
    return (
      <TableGrid className="FlakeTableGrid" bordered={bordered} selectType={selectType}>
        <TableGrid.Head
          showCheckbox={selectType === "checkbox"}
          allSelected={selectType === "checkbox" && selectedItems.length === items.length}
          partialSelected={selectType === "checkbox" && selectedItems.length > 0 && selectedItems.length < items.length}
          onToggleSelection={this.toggleAllSelections}
        >
          <TableGrid.ColumnHeader
            id="id"
            isSorted={false}
            isAscending={isAscending}
            onSortToggle={() => this.onSortToggle("id")}
            {...idColSizes}
          >
            ID
          </TableGrid.ColumnHeader>
          <TableGrid.ColumnHeader
            id="title"
            isSorted={false}
            isAscending={isAscending}
            onSortToggle={() => this.onSortToggle("title")}
            {...titleColSizes}
          >
            Title
          </TableGrid.ColumnHeader>
          <TableGrid.ColumnHeader
            id="content"
            isSorted={false}
            isAscending={isAscending}
            onSortToggle={() => this.onSortToggle("content")}
            {...contentColSizes}
          >
            Content
          </TableGrid.ColumnHeader>
        </TableGrid.Head>
        <TableGrid.Body>{items.map((item, index) => this.renderItemRow(item, index))}</TableGrid.Body>
      </TableGrid>
    );
  }
}

export default DataTableGrid;