import * as React from "react";

import DataRecordCard from "./DataRecordCard";

class DataSetCardView extends React.Component {


  render() {
    const {dataSet, records} = this.props;
    const idText = dataSet.id ?
      <span>{dataSet.id}</span> :
      <span><span className="spinner spinner-xs spinner-inline"/> Creating...</span>;

    return (
      <div className="data-set-card-view">
        <div className="title-container">
          <h3 className="card-title"><i className="fas fa-folder card-title-icon"/>Data Set</h3>
          <div className="attributes-container">
            <div className="field-label">ID:</div><div className="field-value">{idText}</div>
            <div className="field-label">Name:</div><div className="field-value">{dataSet.name}</div>
          </div>
        </div>
        <div className="record-container">
          {records.map((r, index) => <DataRecordCard key={index} record={r}/>)}
        </div>
      </div>
    );
  }
}

export default DataSetCardView;
