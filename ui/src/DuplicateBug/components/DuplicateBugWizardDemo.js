import * as React from "react";
import { connect } from "react-redux";
import Transition from "react-transition-group/Transition";
import classNames from "classnames";
import lodashGet from "lodash/get";
import { Grid, Tab, Tabs } from "patternfly-react";

import AxiosError from "../../AxiosError/components/AxiosError";
import DataSetCardView from "./DataSetCardView";
import DataTableGrid from "./DataTableGrid";
import PredictionCardView from "./PredictionCardView";
import PredictionList from "./PredictionList";
import JsonSample from "../../JsonSample/components/JsonSample";
import CodeSample from "../../CodeSample/components/CodeSample";

import { trainingData, predictionData } from "./sampleData";
import generateCurl from "../../Utilities/generateCurl";
import { updatePredictionUI, createDuplicatePrediction, getDuplicatePrediction, resetDuplicatePrediction } from "../actions";


const pollingInterval = 10000;
const duration = 300;

const defaultStyle = {
  transition: `opacity ${duration}ms ease-in-out`,
  opacity: 0,
  height: 0
};

const transitionStyles = {
  entering: {
    opacity: 0,
    height: 0
  },
  entered: {
    opacity: 1,
    height: "auto"
  },
};


class DuplicateBugWizardDemo extends React.Component {
  componentDidMount() {
    this.startPolling();
  }

  componentWillUnmount() {
    this.stopPolling();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.createPredictionDataRequest && !prevProps.createPredictionDataRequest) {
      this.scrollIntoView(".duplicate-bug-demo-prediction-data");
    }
    if (this.props.createPredictionRequest && !prevProps.createPredictionRequest) {
      this.scrollIntoView(".duplicate-bug-demo-result");
    }
  }

  startPolling = () => {
    this.setState({
      pollingTimer: setInterval(() => this.pollPrediction(), pollingInterval)
    });
  };

  stopPolling = () => {
    if (this.state.pollingTimer) {
      clearInterval(this.state.pollingTimer);
    }
  };

  pollPrediction = () => {
    const {prediction, predictionLoading, predictionAsyncStatus} = this.props;
    if (prediction && prediction.id && !predictionLoading && predictionAsyncStatus !== "success") {
      this.props.getDuplicatePrediction(prediction.id);
    }
  };

  toggleTrainingData = () => {
    const {ui, updateUI} = this.props;
    updateUI({browseTrainingData: !ui.browseTrainingData})
  };

  renderTrainingData = () => {
    const {ui} = this.props;
    let data = <a className="clickable-empty-link code-sample-action" onClick={this.toggleTrainingData}>Show Data
      Records</a>;

    if (ui.browseTrainingData) {
      data = (
        <div>
          <div className="collapsible-heading">
            <h3 className="collapsible-heading-title">Sample Training Data</h3>
            <a className="clickable-empty-link collapsible-heading-action" onClick={this.toggleTrainingData}>Hide</a>
          </div>
          <DataTableGrid data={trainingData.records}/>
        </div>
      )
    }

    return (
      <div className="duplicate-bug-info-section">
        <h2>Using Sample Data and Model</h2>
        <p>For the demonstration, we'll be using pre-created sample data of about 200 records. While convenient,
          generally, you'll be creating your own data sets and models.
        </p>
        {data}
      </div>)
  };

  renderResultsPredictionDataOld = () => {
    const {
      createPredictionDataRequest,
      createPredictionDataResponse,
      createPredictionDataRecordsRequest,
      createPredictionDataRecordsResponse
    } = this.props;

    const dataSetId = lodashGet(createPredictionDataResponse, "data.data.id");
    const dataSetText = dataSetId ? `(Data Set ${dataSetId})` : "";
    let dataRecords = null;
    if (createPredictionDataRecordsResponse) {
      dataRecords = (
        <DataTableGrid
          data={createPredictionDataRecordsResponse.map(r => r.data.data)}
        />);
    } else if (createPredictionDataRecordsRequest) {
      dataRecords = (
        <div>
          <DataTableGrid
            data={createPredictionDataRecordsRequest.body}

          />
          <span className="spinner spinner-xs spinner-inline"/> Creating Data Records
        </div>)
    } else if (createPredictionDataRequest) {
      dataRecords = (
        <div>
          <span className="spinner spinner-xs spinner-inline"/> Creating Data Set
          with <code>name: {createPredictionDataRequest.body.name}</code>
        </div>);
    }

    return (
      <div className="tab-content">
        <h3>Data Set: AI Library Demo (ID: {dataSetText})</h3>
        {dataRecords}
      </div>)
  };

  renderResultsPredictionData = () => {
    const {
      createPredictionDataRequest,
      createPredictionDataResponse,
      createPredictionDataRecordsRequest,
      createPredictionDataRecordsResponse
    } = this.props;

    let dataSet = null;
    if (createPredictionDataResponse) {
      dataSet = {...createPredictionDataResponse.data.data}
    } else if (createPredictionDataRequest) {
      dataSet = {...createPredictionDataRequest.body}
    }

    let dataRecords = [];
    if (createPredictionDataRecordsResponse) {
      dataRecords = createPredictionDataRecordsResponse.map(r => r.data.data);
    } else if (createPredictionDataRecordsRequest) {
      dataRecords = createPredictionDataRecordsRequest.body;
    }

    if (createPredictionDataRequest) {
      return (
        <div className="tab-content">
          <DataSetCardView dataSet={dataSet} records={dataRecords}/>
        </div>);
    }
  };

  renderApiPredictionData = () => {
    const {
      status,
      createPredictionDataRequest,
      createPredictionDataResponse,
      createPredictionDataRecordsRequest,
      createPredictionDataRecordsResponse
    } = this.props;

    if (!createPredictionDataRequest || !createPredictionDataResponse || !createPredictionDataRecordsRequest || !createPredictionDataRecordsResponse) {
      return "";
    }

    const dataSetId = lodashGet(createPredictionDataResponse, "data.data.id");
    let createPredictionDataCurl = generateCurl("POST", status.baseUrl, "api/duplicate/prediction-data", createPredictionDataRequest.body);
    let createPredictionDataRecordCurl = generateCurl(
      "POST",
      status.baseUrl,
      `api/duplicate/prediction-data/${dataSetId}/records`,
      createPredictionDataRecordsRequest.body[0]);

    return (
      <div className="tab-content">
        <Grid.Row>
          <Grid.Col xs={12}>
            <h3>Create a Prediction Data Set</h3>
            <p>As part of this demonstration, we created a new data set.</p>
          </Grid.Col>
        </Grid.Row>
        <Grid.Row>
          <Grid.Col className="sample-request-col" lg={6}>
            <CodeSample title="Request" code={createPredictionDataCurl} language="bash"/>
          </Grid.Col>
          <Grid.Col className="sample-response-col" lg={6}>
            <JsonSample title="Response" object={createPredictionDataResponse.data}/>
          </Grid.Col>
        </Grid.Row>
        <Grid.Row>
          <Grid.Col xs={12}>
            <h3>Create a Prediction Data Record</h3>
            <p>We then added prediction data records as part of that data set.</p>
          </Grid.Col>
        </Grid.Row>
        <Grid.Row>
          <Grid.Col className="sample-request-col" lg={6}>
            <CodeSample title={"Request"} code={createPredictionDataRecordCurl} language="json"/>
          </Grid.Col>
          <Grid.Col className="sample-response-col" lg={6}>
            <JsonSample title={"Response"} object={createPredictionDataRecordsResponse[0].data.data}/>
          </Grid.Col>
        </Grid.Row>
      </div>)
  };

  selectPredictionDataTab = (predictionDataTabActiveKey) => {
    this.props.updateUI({predictionDataTabActiveKey});
  };

  renderPredictionData() {
    const {predictionDataTabActiveKey} = this.props.ui;

    return (
      <div className="duplicate-bug-info-section duplicate-bug-demo-prediction-data">
        <h2>Creating Prediction Data</h2>
        <p>For the demonstration we created a data set to analyze. We then added 4 new bugs, prediction data records, to
          analyze and see if there are likely duplicates.</p>
        <Tabs
          activeKey={predictionDataTabActiveKey}
          onSelect={this.selectPredictionDataTab}
          animation={false}
          id="duplicateBugPredictionDataTabs"
          className="duplicate-bug-demo-tab"
        >
          <Tab eventKey={0} title="Data">
            {this.renderResultsPredictionData()}
          </Tab>
          <Tab eventKey={1} title="API">
            {this.renderApiPredictionData()}
          </Tab>
        </Tabs>
      </div>
    );
  };

  renderResultsPredictionOld = () => {
    const {prediction, createPredictionDataRecordsResponse} = this.props;
    let dataRecords = [];
    if (createPredictionDataRecordsResponse) {
      dataRecords = createPredictionDataRecordsResponse.map(r => r.data.data);
    }
    return (
      <div className="tab-content">
        <PredictionList prediction={prediction || []} records={dataRecords}/>
      </div>)
  };

  renderResultsPrediction = () => {
    const {prediction, createPredictionDataRecordsResponse} = this.props;
    let dataRecords = [];
    if (createPredictionDataRecordsResponse) {
      dataRecords = createPredictionDataRecordsResponse.map(r => r.data.data);
    }
    return (
      <div className="tab-content">
        <PredictionCardView prediction={prediction || []} records={dataRecords}/>
      </div>)
  };

  apiCreatePredictionSection = () => {
    const {status, createPredictionRequest, createPredictionResponse} = this.props;
    let renderArray = [];

    if (!createPredictionRequest) {
      return renderArray;
    }

    let createPredictionCurl = generateCurl("POST", status.baseUrl, "api/duplicate/predictions", createPredictionRequest );

    renderArray.push(
      <Grid.Row>
        <Grid.Col xs={12}>
          <h3>Start the Prediction Job (Async)</h3>
          <p>Now that we have a data set and a model, we can create a prediction to see if there are any likely
            duplicates. Since the job will take some time, the job will run asynchronously.
          </p>
        </Grid.Col>
      </Grid.Row>);

    let response;
    if (createPredictionResponse) {
      response = <JsonSample title="Response" object={createPredictionResponse.data}/>;
    } else {
      response = (
        <div className="duplicate-bug-demo-status alert alert-info">
          <span className="pficon pficon-spinner fa-spin"/> Request in progress...
        </div>);
    }

    renderArray.push(
      <Grid.Row>
        <Grid.Col className="sample-request-col" lg={6}>
          <CodeSample title="Request" code={createPredictionCurl} language="bash"/>
        </Grid.Col>
        <Grid.Col className="sample-response-col" lg={6}>
          {response}
        </Grid.Col>
      </Grid.Row>);

    return renderArray;
  };

  apiPollPredictionSection = () => {
    const {status, createPredictionResponse, getPredictionResponseInProgress, getPredictionResponseComplete} = this.props;
    let renderArray = [];

    if (!createPredictionResponse || (!getPredictionResponseInProgress && getPredictionResponseComplete)) {
      return renderArray;
    }

    let predictionId = lodashGet(createPredictionResponse, "data.data.id");
    let getPredictionCurl = generateCurl("GET", status.baseUrl, `api/duplicate/predictions/${predictionId}?include=records`);

    renderArray.push(
      <Grid.Row>
        <Grid.Col xs={12}>
          <h3>Poll the Async Creation of the Prediction</h3>
          <p>As the asynchronous job runs, we can poll the results every so often. While the job is running, it returns
            an async status of <code>in_progress</code>.  This was the most recent request while the job was in progress.
          </p>
        </Grid.Col>
      </Grid.Row>);


    let response;
    if (getPredictionResponseInProgress) {
      response = <JsonSample title="Response" object={getPredictionResponseInProgress.data}/>;
    } else {
      response = (
        <div className="duplicate-bug-demo-status alert alert-info">
          <span className="pficon pficon-spinner fa-spin"/> Request in progress...
        </div>);
    }

    renderArray.push(
      <Grid.Row>
        <Grid.Col className="sample-request-col" lg={6}>
          <CodeSample title="Request" code={getPredictionCurl} language="bash"/>
        </Grid.Col>
        <Grid.Col className="sample-response-col" lg={6}>
          {response}
        </Grid.Col>
      </Grid.Row>);

    return renderArray;
  };

  apiGetPredictionSection = () => {
    const {status, createPredictionResponse, getPredictionResponse, getPredictionResponseComplete} = this.props;
    let renderArray = [];

    if (!createPredictionResponse || !getPredictionResponse) {
      return renderArray;
    }

    renderArray.push(<Grid.Row>
      <Grid.Col xs={12}>
        <h3>Get the Prediction</h3>
        <p>When the job completes, the get will return the prediction with the async
          status <code>success</code> or <code>failure</code>.
        </p>
      </Grid.Col>
    </Grid.Row>);

    let predictionId = lodashGet(createPredictionResponse, "data.data.id");
    let getPredictionCurl = generateCurl("GET", status.baseUrl, `api/duplicate/predictions/${predictionId}?include=records`);

    if (getPredictionResponseComplete) {
      renderArray.push(
        <Grid.Row>
          <Grid.Col className="sample-request-col" lg={6}>
            <CodeSample title="Request" code={getPredictionCurl} language="bash"/>
          </Grid.Col>
          <Grid.Col className="sample-response-col" lg={6}>
            <JsonSample title="Response (Job Complete)" object={getPredictionResponseComplete.data} showArrayLimit={5}/>
          </Grid.Col>
        </Grid.Row>);
    } else {
      renderArray.push(
        <Grid.Row>
          <Grid.Col className="sample-request-col" lg={12}>
            <div className="duplicate-bug-demo-status alert alert-info">
              <span className="pficon pficon-spinner fa-spin"/> Polling job until analysis is complete...
            </div>
          </Grid.Col>
        </Grid.Row>);
    }

    return renderArray;
  };


  renderApiPrediction = () => {
    return (
      <div className="tab-content">
        {this.apiCreatePredictionSection()}
        {this.apiPollPredictionSection()}
        {this.apiGetPredictionSection()}
      </div>)
  };

  selectPredictionTab = (predictionTabActiveKey) => {
    this.props.updateUI({predictionTabActiveKey});
  };

  renderPrediction = () => {
    const {predictionError} = this.props;
    const {predictionTabActiveKey} = this.props.ui;

    if (predictionError) {
      return (
        <div className="duplicate-bug-info-section duplicate-bug-demo-result">
          <h2>Creating a Prediction</h2>
          <AxiosError error={this.props.predictionError}/>
        </div>)
    }

    return (
      <div className="duplicate-bug-info-section duplicate-bug-demo-result">
        <h2>Creating a Prediction</h2>
        <Tabs
          activeKey={predictionTabActiveKey}
          onSelect={this.selectPredictionTab}
          animation={false}
          id="duplicateBugPredictionTabs"
          className="duplicate-bug-demo-tab"
        >
          <Tab eventKey={0} title="Results">
            {this.renderResultsPrediction()}
          </Tab>
          <Tab eventKey={1} title="API">
            {this.renderApiPrediction()}
          </Tab>
        </Tabs>
      </div>
    );
  };

  pasteSample = (e, bugFormTitle, bugFormContent) => {
    e.preventDefault();
    this.props.updateUI({bugFormTitle, bugFormContent});
  };

  scrollIntoView = (selector) => {
    let elem = document.querySelector(selector);
    if (elem) {
      elem.scrollIntoView({behavior: "smooth"});
    }
  };

  runDemo = async () => {
    const {ui, createPredictionDataRequest} = this.props;

    const title = ui.bugFormTitle;
    const content = ui.bugFormContent;
    const formComplete = title && title.trim() && content && content.trim();
    const inProgress = createPredictionDataRequest;
    const disableRun = inProgress || !formComplete;

    if (disableRun) {
      return;
    }

    this.props.createDuplicatePrediction([{title, content}], "sample");
  };

  resetDemo = async () => {
    const {createPredictionDataRequest} = this.props;

    if (!createPredictionDataRequest) {
      return;
    }


    this.props.resetDuplicatePrediction();
  };

  updateTitle = (event) => {
    if (this.props.createPredictionRequest) {
      return;
    }

    this.props.updateUI({bugFormTitle: event.target.value});
  };

  updateContent = (event) => {
    if (this.props.createPredictionRequest) {
      return;
    }

    this.props.updateUI({bugFormContent: event.target.value});

  };

  renderBugForm = () => {
    const {
      ui,
      createPredictionDataRequest,
      predictionAsyncStatus
    } = this.props;

    const title = ui.bugFormTitle;
    const content = ui.bugFormContent;
    const formComplete = title && title.trim() && content && content.trim();
    const started = !!createPredictionDataRequest;
    const inProgress = createPredictionDataRequest;
    const complete = predictionAsyncStatus !== "in_progress";
    const disableRun = inProgress || !formComplete;

    const stopButtonClasses = classNames("btn duplicate-bug-demo-btn-stop", {
      "disabled": !started,
      "btn-default": complete,
      "btn-danger": inProgress
    });

    const runButtonClasses = classNames("btn duplicate-bug-demo-btn-run btn-primary", {
      "disabled": disableRun
    });

    return (
      <div className="duplicate-bug-info-section">
        <h2>Submit Your Bug</h2>
        <p>Submit your own bug report. The title and content will both be used and compared to the existing bugs.</p>
        <p>Click
          <a className="clickable-empty-link"
             onClick={e => this.pasteSample(e, predictionData.records[0].title, predictionData.records[0].content)}> here </a>
          to fill it out with a sample. You can also
          <a className="clickable-empty-link" onClick={e => this.pasteSample(e, "", "")}> clear </a>
          the form and type in a bug of your own to run against the sample data.</p>
        <form className="sentiment-wizard-contents">
          <div className="form-group required">
            <label className="control-label" htmlFor="bugTitleInput">Title:</label>
            <input className="form-control" id="bugTitleInput" name="bugTitle" type="text"
                   value={title} onChange={this.updateTitle} disabled={inProgress}/>
          </div>
          <div className="form-group required">
            <label className="control-label" htmlFor="bugContentInput">Content:</label>
            <textarea className="form-control" id="bugContentInput" name="bugContent" rows="5"
                      value={content} onChange={this.updateContent} disabled={inProgress}/>
          </div>
        </form>
        <button
          type="button"
          className={stopButtonClasses}
          disabled={false}
          onClick={this.resetDemo}
        ><i className="fa fa-undo"/> Reset
        </button>
        <button
          type="submit"
          className={runButtonClasses}
          disabled={false}
          onClick={this.runDemo}
        ><i className="fa fa-play"/> Run
        </button>
      </div>
    );
  };

  render() {
    const {createPredictionDataRequest, createPredictionRequest} = this.props;

    return (
      <div className="duplicate-bug-wizard-contents duplicate-bug-demo duplicate-bug-success">
        <h1 className="wizard-pane-title">Live Demo</h1>
        <p>For the demonstration, enter your own bug ticket against the sample data and see a list of possible duplicate
          bugs.</p>
        {this.renderTrainingData()}
        {this.renderBugForm()}
        <Transition in={createPredictionDataRequest} timeout={duration}>
          {(state) => (
            <div style={{
              ...defaultStyle,
              ...transitionStyles[state]
            }}>
              {this.renderPredictionData()}
            </div>
          )}
        </Transition>
        <Transition in={createPredictionRequest} timeout={duration}>
          {(state) => (
            <div style={{
              ...defaultStyle,
              ...transitionStyles[state]
            }}>
              {this.renderPrediction()}
            </div>
          )}
        </Transition>
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    ...state.duplicateBugReducer,
    ...state.statusReducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    updateUI: (ui) => {
      dispatch(updatePredictionUI(ui));
    },
    createDuplicatePrediction: (bugs, modelId) => {
      dispatch(createDuplicatePrediction(bugs, modelId));
    },
    getDuplicatePrediction: (id) => {
      dispatch(getDuplicatePrediction(id));
    },
    resetDuplicatePrediction: () => {
      dispatch(resetDuplicatePrediction());
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DuplicateBugWizardDemo);
