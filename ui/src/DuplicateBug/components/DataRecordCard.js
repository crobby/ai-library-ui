import * as React from "react";

import PredictionListItem from "./PredictionListItem";

class DataRecordCard extends React.Component {


  render() {
    const {record} = this.props;
    const idText = record.id ?
      <span className="card-title-extra-info">{record.id}</span> :
      <span className="card-title-extra-info"><span className="spinner spinner-xs spinner-inline"/> Creating...</span>;

    return (
      <div className="data-record-card">
        <h3 className="card-title"><i className="fas fa-bug card-title-icon"/>Data Record</h3>
        <div className="attributes-container">
          <div className="field-label">ID:</div><div className="field-value">{idText}</div>
          <div className="field-label">Title:</div><div className="field-value">{record.title}</div>
          <div className="field-label">Contents:</div><div className="field-value">{record.content}</div>
        </div>

      </div>
    );
  }
}

export default DataRecordCard;
