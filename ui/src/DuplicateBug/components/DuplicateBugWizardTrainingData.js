import React from "react";
import { connect } from "react-redux";

import {
  Grid,
  Tab,
  Tabs
} from "patternfly-react";

import JsonSample from "../../JsonSample/components/JsonSample";
import CodeSample from "../../CodeSample/components/CodeSample";
import generateCurl from "../../Utilities/generateCurl"
import { trainingData } from "./sampleData";
import DataTableGrid from "./DataTableGrid";


function generateTrainingDataCurlCommands(baseUrl) {
  const createdAt = new Date().toISOString();
  const modifiedAt = createdAt;
  let sampleCurlCommands = {};

  sampleCurlCommands.createTrainingDataCurl = generateCurl(
    'POST',
    baseUrl,
    "api/duplicate/training-data");

  sampleCurlCommands.createTrainingDataCurlResponse = {
    metadata: {
      type: "DuplicateBugTrainingData",
      async: {
        status: "success"
      }
    },
    data: {
      id: trainingData.id,
      createdAt,
      modifiedAt
    }
  };

  let trBody = {
    title: "Bug report title",
    content: "Bug report main content"
  };

  sampleCurlCommands.createTrainingRecordCurl = generateCurl(
    'POST',
    baseUrl,
    `api/duplicate/training-data/${trainingData.id}/records`,
    trBody);

  trBody = {
    id: trainingData.records[0].id,
    ...trBody,
    createdAt,
    modifiedAt
  };

  sampleCurlCommands.createTrainingRecordCurlResponse = {
    metadata: {
      type: "DuplicateBugTrainingDataRecord",
      async: {
        status: "success"
      }
    },
    data: trBody
  };

  return sampleCurlCommands;
}

class DuplicateBugWizardTrainingData extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tabActiveKey: 0,
      curlCommands: generateTrainingDataCurlCommands(props.status.baseUrl)
    }
  }

  selectTab = (tabActiveKey) => {
    this.setState({tabActiveKey});
  };

  render() {
    const {tabActiveKey, curlCommands} = this.state;

    return (
      <div className="duplicate-bug-wizard-contents duplicate-bug-sample">
        <Tabs
          activeKey={tabActiveKey}
          onSelect={this.selectTab}
          animation={false}
          id="duplicateResultsTabs"
        >
          <Tab eventKey={0} title="Data">
            <div className="duplicate-bug-info-section">
              <h2>Training Data</h2>
              <p>Training data consists of the <code>title</code> and <code>content</code>.  The record's <code>id</code>
                is generated when the record is created.</p>
              <JsonSample title="Example Training Data Record"
                          object={{
                            id: "uuid-generated-on-creation",
                            title: "Bug report title",
                            content: "Bug report main content"
                          }}/>
              <h3>Using Sample Data Set</h3>
              <p>This is pre-created sample data of about 200 records.</p>
              <DataTableGrid data={trainingData.records}/>
            </div>
          </Tab>
          <Tab eventKey={1} title="API">
            <div className="duplicate-bug-info-section">
              <h2>Using the API - Creating Training Data</h2>
              <p>Data sets and their records are used to train models. Create the set, and then add records to the
                created set.
              </p>
              <p>Since we're using the sample data set, this is an example of creating the sample data set and adding
                records to that set. For your data be sure to replace <code>sample</code> with your data set's unique id
                from the response.
              </p>
              <h3>Create a Training Data Set</h3>
              <Grid.Row>
                <Grid.Col className="sample-request-col" lg={6}>
                  <CodeSample title="Sample Request" code={curlCommands.createTrainingDataCurl} language="bash"/>
                </Grid.Col>
                <Grid.Col className="sample-response-col" lg={6}>
                  <JsonSample title="Sample Response" object={curlCommands.createTrainingDataCurlResponse}/>
                </Grid.Col>
              </Grid.Row>
              <h3>Create a Training Data Record</h3>
              <Grid.Row>
                <Grid.Col className="sample-request-col" lg={6}>
                  <CodeSample title="Sample Request" code={curlCommands.createTrainingRecordCurl} language="bash"/>
                </Grid.Col>
                <Grid.Col className="sample-response-col" lg={6}>
                  <JsonSample title="Sample Response" object={curlCommands.createTrainingRecordCurlResponse}/>
                </Grid.Col>
              </Grid.Row>
            </div>
          </Tab>
        </Tabs>
      </div>
    );
  };
}

function mapStateToProps(state) {
  return state.statusReducer;
}

export default connect(mapStateToProps)(DuplicateBugWizardTrainingData);