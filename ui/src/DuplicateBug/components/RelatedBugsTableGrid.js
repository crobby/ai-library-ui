
import React from "react";
import { TableGrid } from "patternfly-react-extensions";

const rankColSizes = {
  sm: 1
};
const idColSizes = {
  sm: 4
};
const titleColSizes = {
  sm: 7
};

class RelatedBugsTableGrid extends React.Component {


  constructor(props) {
    super(props);

    this.state = {
      sortField: "index",
      isAscending: true,
      items: props.data,
      selectedItem: null,
      selectedField: null,
      selectedItems: []
    }
  }

  renderItemRow = (item, index) => {
    const {selectType} = this.props;
    const {selectedItem, selectedField, selectedItems} = this.state;
    const selected = selectType === "checkbox" ? selectedItems.indexOf(item) >= 0 : selectedItem === item;
    return (
      <TableGrid.Row
        key={index}
        onClick={() => selectType === "row" && this.onSelect(item)}
        selected={(selectType === "row" || selectType === "checkbox") && selected}
        onToggleSelection={() => this.toggleSelection(item)}
      >
        <TableGrid.Col
          {...rankColSizes}
          onClick={() => selectType === "cell" && this.onSelect(item, "index")}
          selected={selectType === "cell" && selected && selectedField === "index"}
        >
          {index + 1}
        </TableGrid.Col>
        <TableGrid.Col
          {...idColSizes}
          onClick={() => selectType === "cell" && this.onSelect(item, "id")}
          selected={selectType === "cell" && selected && selectedField === "id"}
        >
          {item.id.replace(".json", "")}
        </TableGrid.Col>
        <TableGrid.Col
          {...titleColSizes}
          onClick={() => selectType === "cell" && this.onSelect(item, "title")}
          selected={selectType === "cell" && selected && selectedField === "title"}
        >
          {item.title}
        </TableGrid.Col>
      </TableGrid.Row>
    );
  };

  render() {
    const { items, selectedItems, sortField, isAscending } = this.state;
    const { bordered, selectType } = this.props;
    return (
      <TableGrid className="FlakeTableGrid" bordered={bordered} selectType={selectType}>
        <TableGrid.Head
          showCheckbox={selectType === "checkbox"}
          allSelected={selectType === "checkbox" && selectedItems.length === items.length}
          partialSelected={selectType === "checkbox" && selectedItems.length > 0 && selectedItems.length < items.length}
          onToggleSelection={this.toggleAllSelections}
        >
          <TableGrid.ColumnHeader
            id="rank"
            isSorted={sortField === "rank"}
            isAscending={isAscending}
            {...rankColSizes}
          >
            Rank
          </TableGrid.ColumnHeader>
          <TableGrid.ColumnHeader
            id="id"
            isSorted={sortField === "id"}
            isAscending={isAscending}
            {...idColSizes}
          >
            ID
          </TableGrid.ColumnHeader>
          <TableGrid.ColumnHeader
            id="title"
            isSorted={sortField === "title"}
            isAscending={isAscending}
            {...titleColSizes}
          >
            Title
          </TableGrid.ColumnHeader>
        </TableGrid.Head>
        <TableGrid.Body>{items.map((item, index) => this.renderItemRow(item, index))}</TableGrid.Body>
      </TableGrid>
    );
  }
}

export default RelatedBugsTableGrid;