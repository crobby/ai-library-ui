import React from "react";
import { connect } from "react-redux";

import { Grid } from "patternfly-react";

import JsonSample from "../../JsonSample/components/JsonSample";
import CodeSample from "../../CodeSample/components/CodeSample";
import generateCurl from "../../Utilities/generateCurl"
import {trainingData, model} from "./sampleData";


function generateModelCurlCommands(baseUrl) {
  const createdAt = new Date().toISOString();
  const modifiedAt = createdAt;
  let sampleCurlCommands = {};

  sampleCurlCommands.createModelCurl = generateCurl(
    'POST',
    baseUrl,
    "api/duplicate/models",
    { trainingDataId: trainingData.id });

  sampleCurlCommands.createModelCurlResponse = {
    metadata: {
      type: "DuplicateBugModel",
      async: {
        status: "in_progress"
      }
    },
    data: {
      id: model.id,
      trainingDataId: trainingData.id,
      createdAt,
      modifiedAt
    }
  };

  sampleCurlCommands.getModelCurl = generateCurl(
    'GET',
    baseUrl,
    `api/duplicate/models/${model.id}`);

  sampleCurlCommands.getModelCurlResponse = {
    metadata: {
      type: "DuplicateBugModel",
      async: {
        status: "success"
      }
    },
    data: {
      id: model.id,
      trainingDataId: trainingData.id,
      createdAt,
      modifiedAt
    }
  };

  return sampleCurlCommands;
}

class DuplicateBugWizardTraining extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      curlCommands: generateModelCurlCommands(props.status.baseUrl)
    }
  }

  render() {
    const {curlCommands} = this.state;

    return (
      <div className="duplicate-bug-wizard-contents duplicate-bug-sample">
        <div className="duplicate-bug-info-section">
          <h2>Train a Machine Learning Model</h2>
          <p>Now that there's a data set available, we can use it to create a new trained machine learning model.  Model
            training takes more time than a normal request.  This method is asynchronous and will return
            <code>metadata.async.status: "in_progress"</code> when first created.  Afterwards, we can poll for the model
            which will return <code>metadata.async.status: "success"</code> when the model is trained and ready for use.
          </p>
          <h2>Using the API</h2>
          <h3>Create a Trained Model (Async)</h3>
          <Grid.Row>
            <Grid.Col className="sample-request-col" lg={6}>
              <CodeSample title="Sample Request" code={curlCommands.createModelCurl} language="bash"/>
            </Grid.Col>
            <Grid.Col className="sample-response-col" lg={6}>
              <JsonSample title="Sample Response" object={curlCommands.createModelCurlResponse}/>
            </Grid.Col>
          </Grid.Row>
          <h3>Poll the Async Creation of the Model</h3>
          <Grid.Row>
            <Grid.Col className="sample-request-col" lg={6}>
              <CodeSample title="Sample Request" code={curlCommands.getModelCurl} language="bash"/>
            </Grid.Col>
            <Grid.Col className="sample-response-col" lg={6}>
              <JsonSample title="Sample Response" object={curlCommands.getModelCurlResponse}/>
            </Grid.Col>
          </Grid.Row>
        </div>
      </div>
    );
  };
}

function mapStateToProps(state) {
  return state.statusReducer;
}

export default connect(mapStateToProps)(DuplicateBugWizardTraining);