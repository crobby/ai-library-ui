import * as React from "react";

import {
  Col,
  ListView,
  Row
} from "patternfly-react";

import RelatedBugsTableGrid from "./RelatedBugsTableGrid";


class PredictionListItem extends React.Component {
  render() {
    const {bug, initExpanded} = this.props;
    const heading = bug.title;
    const description = bug.content && bug.content.length > 200
      ? bug.content.substring(0, 200)
      : bug.content;

    let relatedBugs = (
      <div className="prediction-pending">
        <h3><span className="spinner spinner-xs spinner-inline"/> Analysis Running...</h3>
      </div>);

    if (bug.duplicateBugs) {
      relatedBugs = <RelatedBugsTableGrid data={bug.duplicateBugs}/>
    }

    return (
      <ListView.Item
        className={"prediction-list-item"}
        actions={<div/>}
        checkboxInput={false}
        leftContent={<ListView.Icon name="bug"/>}
        additionalInfo={[]}
        heading={heading}
        description={description}
        stacked
        initExpanded={initExpanded}
      >
        <Row>
          <Col sm={11}>
            {relatedBugs}
          </Col>
        </Row>
      </ListView.Item>
    );
  }
}

export default PredictionListItem;
