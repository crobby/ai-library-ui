import * as React from "react";

import PredictionRecordCard from "./PredictionRecordCard";

class PredictionCardView extends React.Component {


  render() {
    const {prediction, records} = this.props;
    const idText = prediction.id ?
      <span>{prediction.id}</span> :
      <span><span className="spinner spinner-xs spinner-inline"/> Creating...</span>;

    let items = records;
    if (prediction && prediction.records && prediction.records.length > 0) {
      items = [];
      records.forEach(r => {
        for (let i = 0; i < prediction.records.length; i++) {
          if (prediction.records[i].id === r.id) {
            items.push(prediction.records[i]);
            break;
          }

          //TODO remove
          //TEMP DEBUG
          if (prediction.records[i].title === r.title) {
            items.push(prediction.records[i]);
            break;
          }
          //TEMP DEBUG
        }
      })
    }

    return (
      <div className="prediction-card-view">
        <div className="title-container">
          <h3 className="card-title"><i className="far fa-clone card-title-icon"/>Prediction</h3>
          <div className="attributes-container">
            <div className="field-label">ID:</div><div className="field-value">{idText}</div>
            <div className="field-label">Model ID:</div><div className="field-value">{prediction.modelId}</div>
            <div className="field-label">Data ID:</div><div className="field-value">{prediction.predictionDataId}</div>
          </div>
        </div>
        <div className="record-container">
          {items.map((x, index) => <PredictionRecordCard key={index} num={index} record={x}/>)}
        </div>
      </div>
    );
  }
}

export default PredictionCardView;
