import * as React from "react";
import { connect } from "react-redux";

class FlakeWizardInfo extends React.Component {

  render() {
    const docUrl = "https://gitlab.com/opendatahub/ai-library/tree/master/flake-analysis";
    const {status} = this.props;
    const apiUrl = `${status.baseUrl}/api/flake`;

    return (
      <div className="flake-wizard-contents flake-info">
        <div className="flake-info-section">
          <h2>What is Flake Analysis?</h2>
          <p>Flake analysis aims to identify "flakes", or unreliable false positives, among failures in a testing
            system. By analyzing past failures, and whether or not they were ignored, we can train a machine
            model to recognize similar failures that should be ignored.</p>
        </div>
        <div className="flake-info-section">
          <h2>The API</h2>
          <p>Documentation: <a href={docUrl} target="_blank" rel="noopener noreferrer"><span>{docUrl}</span></a></p>
          <p>API Endpoint: <a href={apiUrl} target="_blank" rel="noopener noreferrer"><span>{apiUrl}</span></a></p>
        </div>
        <div className="flake-info-section">
          <h2>Demo</h2>
          <h3>Create Training Data</h3>
          <p>The AI Library includes the flake analysis learning algorithm, but first we need to create a model using
          some training data.</p>
          <h3>Training a Machine Learning Model</h3>
          <p>The AI Library includes the flake analysis learning algorithm, but first we need to create a model using
            some training data.</p>
          <h3>Preparing Data for Analysis</h3>
          <p>After generating a trained model, we can use this model on a set of data and predict which test failures
          are unreliable and can be ignored.</p>
          <h3>Making a Prediction</h3>
          <p>After generating a trained model, we can use this model on a set of data and predict which test failures
          are unreliable and can be ignored.</p>
        </div>
      </div>)
  };
}

function mapStateToProps(state) {
  return state.statusReducer;
}

export default connect(mapStateToProps)(FlakeWizardInfo);
