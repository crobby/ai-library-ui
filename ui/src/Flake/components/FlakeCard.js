import React from "react";
import { connect } from "react-redux";
import {
  Wizard
} from "patternfly-react";

import scrollToSelector from "../../Utilities/scrollToSelector";
import ServiceTile from "../../ServiceTile/components/ServiceTile"

import FlakeWizardInfo from "./FlakeWizardInfo";
import FlakeWizardTrainingData from "./FlakeWizardTrainingData";
import FlakeWizardTraining from "./FlakeWizardTraining";
import FlakeWizardPredictionData from "./FlakeWizardPredictionData";
import FlakeWizardPrediction from "./FlakeWizardPrediction";

import { modelId, predictionData} from "./sampleData";
import { createFlakePrediction, getFlakePrediction, resetFlakePrediction } from "../actions";

import "./FlakeCard.css"

const pollingInterval = 10000;

class FlakeCard extends React.Component {
  state = {
    showModal: false,
    showLoading: false,
    activeStepIndex: 0
  };

  cardClick = e => {
    e.preventDefault();
    this.openWizard();
  };

  openWizard = () => {
    this.setState({
      showModal: true,
      activeStepIndex: 0
    });
    this.props.resetFlakePrediction();
  };

  closeWizard = () => {
    this.setState({showModal: false});
    this.stopPolling();
    this.props.resetFlakePrediction();
  };

  disableWizardNext = () => {
    return false;
  };

  scrollToTop = () => {
    // hack to make sure we start from the top of each wizard
    scrollToSelector(".wizard-pf-main", 0, 0);
  };

  startPolling() {
    this.setState({
      pollingTimer: setInterval(() => this.pollPrediction(), pollingInterval)
    });
  }

  stopPolling() {
    if (this.state.pollingTimer) {
      clearInterval(this.state.pollingTimer);
    }
  }

  pollPrediction() {
    const {prediction, predictionLoading, predictionAsync} = this.props;
    if (prediction && prediction.id && !predictionLoading && predictionAsync && predictionAsync.status !== "success") {
      this.props.getFlakePrediction(prediction.id);
    }
  };

  onWizardStepChange = newStepIndex => {
    const {prediction} = this.props;

    if (this.state.activeStepIndex === 0 && newStepIndex !== 0 && !prediction) {
      this.props.createFlakePrediction(predictionData, modelId);
      this.startPolling();
    }

    switch (newStepIndex) {
      default:
        this.setState({activeStepIndex: newStepIndex}, this.scrollToTop);
        break;
    }
  };

  render() {
    const {showModal, activeStepIndex} = this.state;
    const wizardNextDisabled = this.disableWizardNext();
    const wizardSteps = [
      {
        title: "Introduction", render: () => (
          <FlakeWizardInfo/>
        )
      },
      {
        title: "Training Data", render: () => (
          <FlakeWizardTrainingData/>
        )
      },
      {
        title: "Training a Model", render: () => (
          <FlakeWizardTraining/>
        )
      },
      {
        title: "Prediction Data", render: () => (
          <FlakeWizardPredictionData/>
        )
      },
      {
        title: "Prediction", render: () => (
          <FlakeWizardPrediction/>
        )
      }
    ];

    return (
      <div>
        <ServiceTile
          key="tile-flake"
          title="Flake Analysis"
          featured={true}
          iconClass="fa fa-snowflake-o"
          description="Identify test failures that can be ignored"
          onClick={this.cardClick}
        />
        <Wizard.Pattern
          className="flake-wizard service-wizard"
          show={showModal}
          backdrop="static"
          onHide={this.closeWizard}
          onExited={this.closeWizard}
          title={<span className="modal-title"><span className="service-wizard-title-icon fa fa-snowflake-o"/> Flake Analysis</span>}
          nextStepDisabled={wizardNextDisabled}
          steps={wizardSteps}
          onStepChanged={this.onWizardStepChange}
          loading={false}
          activeStepIndex={activeStepIndex}
        />
      </div>
    );
  };
}

function mapStateToProps(state) {
  return {
    ...state.flakeReducer,
    ...state.statusReducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    createFlakePrediction: (logs, modelId) => {
      dispatch(createFlakePrediction(logs, modelId));
    },
    getFlakePrediction: (id) => {
      dispatch(getFlakePrediction(id));
    },
    resetFlakePrediction: () => {
      dispatch(resetFlakePrediction());
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FlakeCard);
