import React from "react";
import { connect } from "react-redux";

import {
  Grid,
  Tab,
  Tabs
} from "patternfly-react";

import JsonSample from "../../JsonSample/components/JsonSample";
import TrainingDataTableGrid from "./TrainingDataTableGrid";
import CodeSample from "../../CodeSample/components/CodeSample";
import * as sampleData from "./sampleData";


function generateTrainingCurlCommands(baseUrl) {
  const {generateCurl, trainingDataId, trainingRecordId} = sampleData;
  const createdAt = new Date().toISOString();
  const modifiedAt = createdAt;
  let sampleCurlCommands = {};

  sampleCurlCommands.createTrainingDataCurl = generateCurl(
    'POST',
    baseUrl,
    "api/flake/training-data");

  sampleCurlCommands.createTrainingDataCurlResponse = {
    metadata: {
      type: "FlakeTrainingData",
      async: {
        status: "success"
      }
    },
    data: {
      id: trainingDataId,
      createdAt,
      modifiedAt
    }
  };

  let trBody = {
    status: "failure",
    label: "test: abc",
    log: "log text",
    flake: true
  };

  sampleCurlCommands.createTrainingRecordCurl = generateCurl(
    'POST',
    baseUrl,
    `api/flake/training-data/${trainingDataId}/records`,
    trBody);

  trBody = {
    id: trainingRecordId,
    ...trBody,
    createdAt,
    modifiedAt
  };

  sampleCurlCommands.createTrainingRecordCurlResponse = {
    metadata: {
      type: "FlakeTrainingDataRecord",
      async: {
        status: "success"
      }
    },
    data: trBody
  };

  return sampleCurlCommands;
}


const failureExample  = {
    status: "failure",
    label: "pull 1 fd4f5a test: sample",
    log: "# testSample Trace: Error: Real failure",
    flake: false
};

const flakeExample = {
    status: "failure",
    label: "pull 2 681ef3 test: sample",
    log: "# testSample Trace: Error: Ignore me",
    flake: true
};

class FlakeWizardTrainingData extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tabActiveKey: 0,
      curlCommands: generateTrainingCurlCommands(props.status.baseUrl)
    }
  }

  selectTab = (tabActiveKey) => {
    this.setState({tabActiveKey});
  };

  render() {
    const {tabActiveKey, curlCommands} = this.state;

    return (
      <div className="flake-wizard-contents flake-sample">
        <Tabs
          activeKey={tabActiveKey}
          onSelect={this.selectTab}
          animation={false}
          id="flakeResultsTabs"
        >
          <Tab eventKey={0} title="Data">
            <div className="flake-info-section">
              <h2>Training Data</h2>
              <p>The training data consists of json representations of the logs that contain both meaningful test
                failures as well as failures that were "flakes" that can be ignored. When submitting test data, mark
                failures as either <code>flake: true</code> or <code>flake: false</code>.
              </p>
              <JsonSample title="Example Training Data Record - Meaningful Test Failure"
                          object={failureExample}/>
              <JsonSample title="Example Training Data Record - Unimportant Test Failure (Flake)"
                          object={flakeExample}/>
              <h3>Sample Training Data Records (1-10 of 200)</h3>
              <p>For the demonstration, we'll be using pre-created sample data of about 200 records.  While convenient,
                generally, you'll be creating your own data sets and models.
              </p>
              <TrainingDataTableGrid/>
            </div>
          </Tab>
          <Tab eventKey={1} title="API">
            <div className="flake-info-section">
              <h2>Using the API - Creating Training Data</h2>
              <p>Data sets and their records are used to train models. Create the set, and then add records to the
                created set.
              </p>
              <p>Since we're using the sample data set, this is an example of creating the sample data set and adding
                records to that set. For your data be sure to replace <code>sample</code> with your data set's unique id
                from the response.
              </p>
              <h3>Create a Training Data Set</h3>
              <Grid.Row>
                <Grid.Col className="sample-request-col" lg={6}>
                  <CodeSample title="Sample Request" code={curlCommands.createTrainingDataCurl} language="bash"/>
                </Grid.Col>
                <Grid.Col className="sample-response-col" lg={6}>
                  <JsonSample title="Sample Response" object={curlCommands.createTrainingDataCurlResponse}/>
                </Grid.Col>
              </Grid.Row>
              <h3>Create a Training Data Record</h3>
              <Grid.Row>
                <Grid.Col className="sample-request-col" lg={6}>
                  <CodeSample title="Sample Request" code={curlCommands.createTrainingRecordCurl} language="bash"/>
                </Grid.Col>
                <Grid.Col className="sample-response-col" lg={6}>
                  <JsonSample title="Sample Response" object={curlCommands.createTrainingRecordCurlResponse}/>
                </Grid.Col>
              </Grid.Row>
            </div>
          </Tab>
        </Tabs>
      </div>
    );
  };
}

function mapStateToProps(state) {
  return state.statusReducer;
}

export default connect(mapStateToProps)(FlakeWizardTrainingData);