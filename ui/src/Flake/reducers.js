import lodashGet from "lodash/get";

import {
  RESET_FLAKE_PREDICTION,
  CREATE_FLAKE_PREDICTION_DATA_FULFILLED,
  CREATE_FLAKE_PREDICTION_RECORD_FULFILLED,
  GET_FLAKE_PREDICTION_PENDING,
  GET_FLAKE_PREDICTION_FULFILLED,
  GET_FLAKE_PREDICTION_REJECTED,
  CREATE_FLAKE_PREDICTION_PENDING,
  CREATE_FLAKE_PREDICTION_FULFILLED,
  CREATE_FLAKE_PREDICTION_REJECTED
} from "./actions";


const initialState = {
  prediction: null,
  predictionAsync: null,
  createPredictionDataResponse: null,
  createPredictionRecordResponse: null,
  getPredictionDate: null,
  getPredictionResponse: null,
  createPredictionResponse: null,
  predictionLoading: false,
  predictionError: null
};


export const flakeReducer = (state = initialState, action) => {
  switch (action.type) {
    case RESET_FLAKE_PREDICTION:
      return initialState;

    case CREATE_FLAKE_PREDICTION_DATA_FULFILLED:
      return {
        ...state,
        createPredictionDataResponse: lodashGet(action, "payload.response.data")
      };

    case CREATE_FLAKE_PREDICTION_RECORD_FULFILLED:
      return {
        ...state,
        createPredictionRecordResponse: lodashGet(action, "payload.response.data")
      };

    case CREATE_FLAKE_PREDICTION_PENDING:
      return {
        ...state,
        prediction: null,
        predictionAsync: null,
        createPredictionDataResponse: null,
        createPredictionRecordResponse: null,
        createPredictionResponse: null,
        predictionLoading: true,
        predictionError: null
      };
    case CREATE_FLAKE_PREDICTION_FULFILLED:
      return {
        ...state,
        prediction: lodashGet(action, "payload.response.data.data"),
        predictionAsync: lodashGet(action, "payload.response.data.metadata.async"),
        createPredictionResponse: lodashGet(action, "payload.response.data"),
        predictionLoading: false,
        predictionError: null
      };
    case CREATE_FLAKE_PREDICTION_REJECTED:
      console.error(lodashGet(action, "payload.error"));
      return {
        ...state,
        prediction: null,
        predictionAsync: null,
        createPredictionResponse: null,
        predictionLoading: false,
        predictionError: lodashGet(action, "payload.error")
      };

    case GET_FLAKE_PREDICTION_PENDING:
      return {
        ...state,
        getPredictionDate: action.payload.requestDate,
        predictionLoading: true,
        predictionError: null
      };
    case GET_FLAKE_PREDICTION_FULFILLED:
      return {
        ...state,
        prediction: lodashGet(action, "payload.response.data.data"),
        predictionAsync: lodashGet(action, "payload.response.data.metadata.async"),
        getPredictionResponse: lodashGet(action, "payload.response.data"),
        predictionLoading: false,
        predictionError: null
      };
    case GET_FLAKE_PREDICTION_REJECTED:
      console.error(action.payload.error);
      return {
        ...state,
        prediction: null,
        predictionAsync: null,
        getPredictionResponse: null,
        predictionLoading: false,
        predictionError: lodashGet(action, "payload.error")
      };
    default:
      return state;
  }
};
