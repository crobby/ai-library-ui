import lodashGet from "lodash/get";

import {
  GET_SENTIMENT_PENDING, GET_SENTIMENT_FULFILLED, GET_SENTIMENT_REJECTED, GET_SENTIMENT_IGNORED,
  CREATE_SENTIMENT_PENDING, CREATE_SENTIMENT_FULFILLED, CREATE_SENTIMENT_REJECTED
} from "./actions";

const initialState = {
  sentiment: null,
  sentimentAsync: null,
  getSentimentResponse: null,
  createSentimentResponse: null,
  sentimentLoading: false,
  sentimentError: null
};

export const sentimentReducer = (state = initialState, action) => {
  switch (action.type) {    
    case GET_SENTIMENT_PENDING:
    return {
      ...state,
      getSentimentResponse: null,
      sentimentLoading: true,
      sentimentError: null
    };
    case GET_SENTIMENT_FULFILLED:
      return {
        ...state,
        sentiment: lodashGet(action, "payload.response.data.data"),
        sentimentAsync: lodashGet(action, "payload.response.data.metadata.async"),
        getSentimentResponse: lodashGet(action, "payload.response.data"),
        sentimentLoading: false,
        sentimentError: null,
      };
    case GET_SENTIMENT_REJECTED:
      console.error(action.payload.error);
      return {
        ...state,
        sentiment: null,
        sentimentAsync: null,
        getSentimentResponse: null,
        sentimentLoading: false,
        sentimentError: lodashGet(action, "payload.error")
      };
    case GET_SENTIMENT_IGNORED:
      return {
        ...state,
        sentimentLoading: false
      };
    case CREATE_SENTIMENT_PENDING:
      return {
        ...state,
        sentiment: null,
        sentimentAsync: null,
        createSentimentResponse: null,
        sentimentLoading: true,
        sentimentError: null
      };
    case CREATE_SENTIMENT_FULFILLED:
      return {
        ...state,
        sentiment: lodashGet(action, "payload.response.data.data"),
        sentimentAsync: lodashGet(action, "payload.response.data.metadata.async"),
        createSentimentResponse: lodashGet(action, "payload.response.data"),
        sentimentLoading: false,
        sentimentError: null,
      };
    case CREATE_SENTIMENT_REJECTED:
      console.error(lodashGet(action, "payload.error"));
      return {
        ...state,
        sentiment: null,
        sentimentAsync: null,
        createSentimentResponse: null,
        sentimentLoading: false,
        sentimentError: lodashGet(action, "payload.error")
      };
    default:
      return state;
  }
};
