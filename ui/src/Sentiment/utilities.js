import urljoin from "url-join";

export function calculateSentimentNumber(sentimentItem) {
  let total = sentimentItem.count * 4;
  let s = sentimentItem.sentiment;
  return ((s.negative || 0) + ((s.neutral || 0) * 2) + ((s.positive || 0) * 3) + ((s.verypositive || 0) * 4)) / total
}

export function calculateSentimentPercent(sentimentItem) {
  return Math.round(calculateSentimentNumber(sentimentItem) * 200) - 100;
}

export function calculateSentiment(sentimentItem) {
  const sentimentPercent = calculateSentimentNumber(sentimentItem);

  if (sentimentPercent > 0.8) {
    return "verypositive"
  } else if (sentimentPercent > 0.6) {
    return "positive";
  } else if (sentimentPercent > 0.4) {
    return "neutral";
  } else if (sentimentPercent > 0.2) {
    return "negative"
  }
  return "verynegative"
}

export function generateSampleData(baseUrl) {
  const id = "abc123";
  const entity = "Red Hat";
  const text = `I love ${entity}`;

  const postResponse = (
    {
      metaData: {
        async: {
          status: "in_progress"
        }
      },
      data: {
        id: id,
      }
    });

  const getResponse = (
    {
      metaData: {
        async: {
          status: "success"
        }
      },
      data: {
        id: id,
        entities: [
          {
            category: [
              "ORG"
            ],
            count: 1,
            name: "Red Hat",
            sentiment: {
              positive: 1
            }
          }
        ]
      }
    });

  return {
    createTrainingDataCurl: generatePostCurl(baseUrl, text),
    postResponse,
    getCurl: generateGetCurl(baseUrl, id),
    getResponse,
  };
}

export function generatePostCurl(baseUrl, text) {
  const url = urljoin(baseUrl, "/api/sentiment");
  const jsonText = JSON.stringify(text);
  return (
    `curl '${url}' \\
  -X POST \\
  -H 'Content-Type: application/json' \\
  -H 'Accept: application/json' \\
  -d '{"text": ${jsonText}}'
`);
}

export function generateGetCurl(baseUrl, id) {
  const url = urljoin(baseUrl, "/api/sentiment", id);
  return (
    `curl '${url}' \\
  -X GET \\
  -H 'Accept: application/json'
`);
}


export const sampleProductReview = (
`One of the hallmarks of Red Hat Enterprise Linux is that it overwhelmingly favors stability over currency. As such, RHEL generally ships with packages and frameworks that are years behind the current releases. This is by design, to ensure that the RHEL distribution is as solid as possible. As an example, Red Hat's slow and steady approach saved RHEL 6.4 users from the OpenSSL Heartbleed vulnerability because all RHEL versions up to and including that version shipped with a two-year-old version of OpenSSL that was not affected.

If you follow the Fedora distribution, which serves as the icebreaker for the more stable RHEL distribution, you've seen many changes coming down the pike for RHEL 7. Many of these changes are the most fundamental we've seen in quite some time. Several are to be heralded, but others -- notably the replacement of Init and Upstart with Systemd -- are likely to chafe longtime RHEL users and potentially curb adoption.


What's new in RHEL 7
There is a long list of changes in RHEL 7, but only a few are fundamental. RHEL 7 now uses Systemd rather than Init scripts for service startup and management -- more on that later. The new default file system is XFS rather than Ext4, with support of XFS file systems up to 500TB in size. To that end, RHEL 7 now supports Ext4 file systems as large as 50TB.

Linux containers get a front-row seat in the form of Docker. RHEL can now perform cross-domain trusts with Microsoft Active Directory, so users can authenticate to Linux resources with Active Directory accounts without the need for synchronization.

RHEL 7 also includes new monitoring and performance tools. For instance, the Performance Co-Pilot (PCP) provides a new API for importing, exporting, and processing performance data, while the Tuned daemon provides dynamic system performance tuning.

On the inside, RHEL 7 incorporates enhanced NUMA affinity features that optimize performance on a per-process level by aligning processor affinity to RAM location, reducing cross-node communication and improving process performance.

RHEL 7 offers tighter integration with the VMware vSphere hypervisor via 3D graphics drivers for hardware acceleration with OpenGL and X11, and Open Virtual Machine Tools, an open source implementation of VMware Tools that is now a maintained package.

Open Linux Management Infrastructure (OpenLMI) is now supported. OpenLMI is a framework that allows for common configuration, management, and monitoring of hardware and software through a remote connection. It provides a standard API that can be used by any compliant controller to make changes to the server configuration or to monitor the system.

Other changes include the use of Chrony versus the historical Network Time Protocol daemon for time synchronization, support for 40GB interfaces, structured logging, and low-latency sockets. A new firewall management interface, Firewalld, now permits firewall configuration changes without restarting.

None of these changes or additions will come as much of a surprise to anyone who's been working with Red Hat's Fedora distribution. But those who working exclusively within the RHEL 5 and RHEL 6 ecosystems are in for a jolt.

Brace for impact 
Of the myriad changes found in RHEL 7, a few are certain to cause consternation. First and foremost of those is the move to the Systemd system and process manager. This represents a major departure from Red Hat's -- and Linux's -- history and from the tried-and-true Unix philosophy of using simple, modular tools for critical infrastructure components. Systemd replaces the simplicity of Init scripts with a major management system that offers new features and capabilities but adds significant complexity. 

Some of the benefits to Systemd are the parallelized service startup at boot and centralized service management -- and it certainly shortens boot times.

However, there are decades of admin reflexes to overcome by introducing Systemd, and those tasked with maintaining servers running RHEL 6 and RHEL 7 releases will quickly tire of the significant administrative differences between them. Red Hat has replicated many original commands to Systemd commands to address this issue (see the Fedora project's SysVinit to Systemd Cheatsheet). But at the heart of the matter, an extremely fundamental part of RHEL server administration is now wildly altered.

To take one example, for 20 years we've been able to issue the chkconfig -list command to show what services are set to start and at what run level. That command is now systemctl list-unit-files --type=service. For the moment, chkconfig -list still works, but chides you for not using the systemctl call. In /etc/init.d you'll find only a few scripts and a README.

Both sides of the Systemd divide have their adherents, but in RHEL 7, the Systemd argument has clearly won. I believe, however, that this will ultimately rankle many veteran Linux admins, and we may be on the road to a real schism in the RHEL community and in the Linux world at large.

Smoother sailing
RHEL7 will integrate Docker, the Linux containers solution. Docker is built around the Linux kernel-based virtualization method that permits multiple, isolated virtual systems, or containers, to run on a single host system. Docker makes it easy to deploy applications and services inside containers and move them between host systems without requiring specific dependencies or package installations on the target host.

For example, you could create a container on an Ubuntu server that's running a Memcached service and copy that container to an RHEL server where it would run without alteration. Linux containers and Docker can also run on physical, virtual, or cloud infrastructures, generally without requiring anything more than the Docker binary installed on the host.

Docker-managed containerization is a big deal for computing in general, and the quick adoption in RHEL 7 shows that Red Hat is interested in getting on the forefront of this change, rather than backing into it in a later release.

Direct support for Active Directory authentication is another significant update, one that may cause more than a few environments to finally ditch NIS and existing LDAP authentication mechanisms. RHEL 7 can now function with cross-domain trusts to Microsoft Active Directory. This means that a user existing only in Active Directory can authenticate to an RHEL 7 server without requiring any synchronization of user data between the realms.

Thus, environments that have been maintaining multiple authentication mechanisms for their Windows and Linux infrastructures can now combine them without jumping through too many hoops. There are many shops that still run NIS on Linux, either maintaining a completely separate authentication realm, or using one of several rather funky methods of combining the two (such as identity synchronization or using a Windows server as the NIS master).

The addition of Performance Co-Pilot (PCP) should also find many supporters. PCP can collect all kinds of performance metrics on a server and make them available to any local or remote viewer, even running on other platforms. PCP can also be used to provide detailed information on application performance. Thorough use of PCP will make troubleshooting intractable server-side problems easier and offer heightened visibility into the operating state of a server.

Finally, the graphical installation tool Anaconda has received a face-lift. It's much flatter, allowing all pertinent configuration elements to be set within one screen, rather than through a series of screens separated by Next buttons. Within a few clicks you can configure the system as you require, then click Install and walk away while that work is done.

On the downside, the package selection is somewhat restricting, separating certain packages by base server selection. For instance, you can't easily select MariaDB server and client in the Web server grouping, so selecting the elements of a LAMP server will need to be done after install.

That said, the new installer is clean and slick, and let's face it -- we're not likely to use the installer much these days. We'll create some templates or images and use those.

RHEL 7 is a fairly significant departure from the expected full-revision release from Red Hat. This is not merely a reskinning of the previous release with updated packages, a more modern kernel, and some new toolkits and widgets. This is a very different release than RHEL 6 in any form, mostly due to the move to Systemd.

Though this change has been visible for some time, it will still cause integration problems in a large number of sites with a significant RHEL installed base. You can expect the adoption of RHEL 7 to be slowed quite a bit in these places, which may push out the lifecycle of RHEL 5 and RHEL 6 longer than Red Hat may like.`
);

export const sampleSpeech = (
`President Hoover, Mr. Chief Justice, my friends:

This is a day of national consecration. And I am certain that on this day my fellow Americans expect that on my induction into the Presidency I will address them with a candor and a decision which the present situation of our people impels. This is preeminently the time to speak the truth, the whole truth, frankly and boldly. Nor need we shrink from honestly facing conditions in our country today. This great Nation will endure as it has endured, will revive and will prosper. So, first of all, let me assert my firm belief that the only thing we have to fear is fear itself—nameless, unreasoning, unjustified terror which paralyzes needed efforts to convert retreat into advance. In every dark hour of our national life a leadership of frankness and vigor has met with that understanding and support of the people themselves which is essential to victory. I am convinced that you will again give that support to leadership in these critical days.

In such a spirit on my part and on yours we face our common difficulties. They concern, thank God, only material things. Values have shrunken to fantastic levels; taxes have risen; our ability to pay has fallen; government of all kinds is faced by serious curtailment of income; the means of exchange are frozen in the currents of trade; the withered leaves of industrial enterprise lie on every side; farmers find no markets for their produce; the savings of many years in thousands of families are gone.

More important, a host of unemployed citizens face the grim problem of existence, and an equally great number toil with little return. Only a foolish optimist can deny the dark realities of the moment.

Yet our distress comes from no failure of substance. We are stricken by no plague of locusts. Compared with the perils which our forefathers conquered because they believed and were not afraid, we have still much to be thankful for. Nature still offers her bounty and human efforts have multiplied it. Plenty is at our doorstep, but a generous use of it languishes in the very sight of the supply. Primarily this is because rulers of the exchange of mankind's goods have failed through their own stubbornness and their own incompetence, have admitted their failure, and have abdicated. Practices of the unscrupulous money changers stand indicted in the court of public opinion, rejected by the hearts and minds of men.

True they have tried, but their efforts have been cast in the pattern of an outworn tradition. Faced by failure of credit they have proposed only the lending of more money. Stripped of the lure of profit by which to induce our people to follow their false leadership, they have resorted to exhortations, pleading tearfully for restored confidence. They know only the rules of a generation of self-seekers. They have no vision, and when there is no vision the people perish.

The money changers have fled from their high seats in the temple of our civilization. We may now restore that temple to the ancient truths. The measure of the restoration lies in the extent to which we apply social values more noble than mere monetary profit.

Happiness lies not in the mere possession of money; it lies in the joy of achievement, in the thrill of creative effort. The joy and moral stimulation of work no longer must be forgotten in the mad chase of evanescent profits. These dark days will be worth all they cost us if they teach us that our true destiny is not to be ministered unto but to minister to ourselves and to our fellow men.

Recognition of the falsity of material wealth as the standard of success goes hand in hand with the abandonment of the false belief that public office and high political position are to be valued only by the standards of pride of place and personal profit; and there must be an end to a conduct in banking and in business which too often has given to a sacred trust the likeness of callous and selfish wrongdoing. Small wonder that confidence languishes, for it thrives only on honesty, on honor, on the sacredness of obligations, on faithful protection, on unselfish performance; without them it cannot live. Restoration calls, however, not for changes in ethics alone. This Nation asks for action, and action now.

Our greatest primary task is to put people to work. This is no unsolvable problem if we face it wisely and courageously. It can be accomplished in part by direct recruiting by the Government itself, treating the task as we would treat the emergency of a war, but at the same time, through this employment, accomplishing greatly needed projects to stimulate and reorganize the use of our natural resources.

Hand in hand with this we must frankly recognize the overbalance of population in our industrial centers and, by engaging on a national scale in a redistribution, endeavor to provide a better use of the land for those best fitted for the land. The task can be helped by definite efforts to raise the values of agricultural products and with this the power to purchase the output of our cities. It can be helped by preventing realistically the tragedy of the growing loss through foreclosure of our small homes and our farms. It can be helped by insistence that the Federal, State, and local governments act forthwith on the demand that their cost be drastically reduced. It can be helped by the unifying of relief activities which today are often scattered, uneconomical, and unequal. It can be helped by national planning for and supervision of all forms of transportation and of communications and other utilities which have a definitely public character. There are many ways in which it can be helped, but it can never be helped merely by talking about it. We must act and act quickly.

Finally, in our progress toward a resumption of work we require two safeguards against a return of the evils of the old order: there must be a strict supervision of all banking and credits and investments, so that there will be an end to speculation with other people's money; and there must be provision for an adequate but sound currency.

These are the lines of attack. I shall presently urge upon a new Congress, in special session, detailed measures for their fulfillment, and I shall seek the immediate assistance of the several States.

Through this program of action we address ourselves to putting our own national house in order and making income balance outgo. Our international trade relations, though vastly important, are in point of time and necessity secondary to the establishment of a sound national economy. I favor as a practical policy the putting of first things first. I shall spare no effort to restore world trade by international economic readjustment, but the emergency at home cannot wait on that accomplishment.

The basic thought that guides these specific means of national recovery is not narrowly nationalistic. It is the insistence, as a first considerations, upon the interdependence of the various elements in and parts of the United States-a recognition of the old and permanently important manifestation of the American spirit of the pioneer. It is the way to recovery. It is the immediate way. It is the strongest assurance that the recovery will endure.

In the field of world policy I would dedicate this Nation to the policy of the good neighbor—the neighbor who resolutely respects himself and, because he does so, respects the rights of others—the neighbor who respects his obligations and respects the sanctity of his agreements in and with a world of neighbors.

If I read the temper of our people correctly, we now realize as we have never realized before our interdependence on each other; that we cannot merely take but we must give as well; that if we are to go forward, we must move as a trained and loyal army willing to sacrifice for the good of a common discipline, because without such discipline no progress is made, no leadership becomes effective. We are, I know, ready and willing to submit our lives and property to such discipline, because it makes possible a leadership which aims at a larger good. This I propose to offer, pledging that the larger purposes will bind upon us all as a sacred obligation with a unity of duty hitherto evoked only in time of armed strife.

With this pledge taken, I assume unhesitatingly the leadership of this great army of our people dedicated to a disciplined attack upon our common problems.

Action in this image and to this end is feasible under the form of government which we have inherited from our ancestors. Our Constitution is so simple and practical that it is possible always to meet extraordinary needs by changes in emphasis and arrangement without loss of essential form. That is why our constitutional system has proved itself the most superbly enduring political mechanism the modern world has produced. It has met every stress of vast expansion of territory, of foreign wars, of bitter internal strife, of world relations.

It is to be hoped that the normal balance of Executive and legislative authority may be wholly adequate to meet the unprecedented task before us. But it may be that an unprecedented demand and need for undelayed action may call for temporary departure from that normal balance of public procedure.

I am prepared under my constitutional duty to recommend the measures that a stricken Nation in the midst of a stricken world may require. These measures, or such other measures as the Congress may build out of its experience and wisdom, I shall seek, within my constitutional authority, to bring to speedy adoption.

But in the event that the Congress shall fail to take one of these two courses, and in the event that the national emergency is still critical, I shall not evade the clear course of duty that will then confront me. I shall ask the Congress for the one remaining instrument to meet the crisis—broad Executive power to wage a war against the emergency, as great as the power that would be given to me if we were in fact invaded by a foreign foe.

For the trust reposed in me I will return the courage and the devotion that befit the time. I can do no less.

We face the arduous days that lie before us in the warm courage of national unity; with the clear consciousness of seeking old and precious moral values; with the clean satisfaction that comes from the stern performance of duty by old and young alike. We aim at the assurance of a rounded and permanent national life.

We do not distrust the future of essential democracy. The people of the United States have not failed. In their need they have registered a mandate that they want direct, vigorous action. They have asked for discipline and direction under leadership. They have made me the present instrument of their wishes. In the spirit of the gift I take it.

In this dedication of a Nation we humbly ask the blessing of God. May He protect each and every one of us. May He guide me in the days to come.`
)

