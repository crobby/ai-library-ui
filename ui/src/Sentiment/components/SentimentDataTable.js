import React from "react";
import { orderBy } from "lodash";
import * as sort from "sortabular";
import * as resolve from "table-resolver";

import {
  customHeaderFormattersDefinition,
  defaultSortingOrder,
  sortableHeaderCellFormatter,
  tableCellFormatter,
  Table,
  TABLE_SORT_DIRECTION
} from "patternfly-react";

import { compose } from "recompose";
import { calculateSentimentPercent } from "../utilities";
import TableCell from "patternfly-react/dist/esm/components/Table/TableCell";

class SentimentDataTable extends React.Component {
  constructor(props) {
    super(props);

    const sortingColumns =    {
      count: {
        direction: TABLE_SORT_DIRECTION.DESC,
          position: 0
      }
    };

    const getSortingColumns = () => this.state.sortingColumns || {};

    const sortableTransform = sort.sort({
      getSortingColumns,
      onSort: selectedColumn => {
        this.setState({
          sortingColumns: sort.byColumn({
            sortingColumns: this.state.sortingColumns,
            sortingOrder: defaultSortingOrder,
            selectedColumn
          })
        });
      },
      strategy: sort.strategies.byProperty
    });

    const sortingFormatter = sort.header({
      sortableTransform,
      getSortingColumns,
      strategy: sort.strategies.byProperty
    });

    const sentimentCellFormatter = value => {

      let sentimentDescription = "Very Negative";
      if (value > 60) {
        sentimentDescription = "Very Positive"
      } else if (value > 20) {
        sentimentDescription = "Positive"
      } else if (value > -20) {
        sentimentDescription = "Neutral"
      } else if (value > -60) {
        sentimentDescription = "Negative"
      }

      return React.createElement(
        TableCell,
        null,
        <span>{value}% ({sentimentDescription})</span>
      );
    };


    const columns = [
      {
        property: "name",
        header: {
          label: "Entity",
          props: {
            index: 0,
            rowSpan: 1,
            colSpan: 1,
            sort: true
          },
          transforms: [sortableTransform],
          formatters: [sortingFormatter],
          customFormatters: [sortableHeaderCellFormatter]
        },
        cell: {
          props: {
            index: 0
          },
          formatters: [tableCellFormatter]
        }
      },
      {
        property: "count",
        header: {
          label: "Count",
          props: {
            index: 1,
            rowSpan: 1,
            colSpan: 1,
            sort: true
          },
          transforms: [sortableTransform],
          formatters: [sortingFormatter],
          customFormatters: [sortableHeaderCellFormatter]
        },
        cell: {
          props: {
            index: 1
          },
          formatters: [tableCellFormatter]
        }
      },
      {
        property: "category",
        header: {
          label: "Category",
          props: {
            index: 2,
            rowSpan: 1,
            colSpan: 1,
            sort: true
          },
          transforms: [sortableTransform],
          formatters: [sortingFormatter],
          customFormatters: [sortableHeaderCellFormatter]
        },
        cell: {
          props: {
            index: 2
          },
          formatters: [tableCellFormatter]
        }
      },
      {
        property: "sentiment",
        header: {
          label: "Sentiment Value",
          props: {
            index: 3,
            rowSpan: 1,
            colSpan: 1,
            sort: true
          },
          transforms: [sortableTransform],
          formatters: [sortingFormatter],
          customFormatters: [sortableHeaderCellFormatter]
        },
        cell: {
          props: {
            index: 3
          },
          formatters: [sentimentCellFormatter]
        }
      }
    ];

    // enable PF custom header formatters
    this.customHeaderFormatters = customHeaderFormattersDefinition;

    this.state = { sortingColumns, columns };
  }

  render() {
    const { sortingColumns, columns } = this.state;

    const rows = sentimentData2Rows(this.props.data);

    const sortedRows = compose(
      sort.sorter({
        columns,
        sortingColumns,
        sort: orderBy,
        strategy: sort.strategies.byProperty
      })
    )(rows);

    return (
      <div className="SentimentDataTable">
        <Table.PfProvider
          striped
          bordered
          hover
          dataTable
          columns={columns}
          components={{
            header: {
              cell: cellProps => {
                return this.customHeaderFormatters({
                  cellProps,
                  columns,
                  sortingColumns
                });
              }
            }
          }}
        >
          <Table.Header headerRows={resolve.headerRows({columns})}/>
          <Table.Body
            rows={sortedRows}
            rowKey="name"
            onRow={() => {
              return {
                role: "row"
              };
            }}
          />
        </Table.PfProvider>
      </div>
    );
  }
}

function sentimentData2Rows(data) {
  return data.map(sentimentItem2Row);
}

function sentimentItem2Row(item) {
  return {
    name: item.name,
    count: item.count,
    category: item.category[0],
    sentiment: calculateSentimentPercent(item),
    verynegative: item.sentiment.verynegative || 0,
    negative: item.sentiment.negative || 0,
    neutral: item.sentiment.neutral || 0,
    positive: item.sentiment.positive || 0,
    verypositive: item.sentiment.verypositive || 0,
  };
}

export default SentimentDataTable;