const utilities = require("./utilities");

const _init = () => {
  let url, basicAuthHeader;
  try {
    url = utilities.getEnv("SENTIMENT_ANALYSIS_URL");

    const username = utilities.getEnv("AI_LIBRARY_USERNAME");
    const password = utilities.getEnv("AI_LIBRARY_PASSWORD");
    basicAuthHeader = utilities.generateAuthHeader(username, password);
  }
  catch (e) {
    console.error(e);
    return {};
  }

  return {url, basicAuthHeader};
};

module.exports = _init();